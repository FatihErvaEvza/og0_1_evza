
public class Buch implements Comparable <Buch>{
	
	private String name;
	private String buchname;
	private String isbn;
	
	public Buch (String name, String buchname, String isbn) {
		this.setName(name);
		this.setBuchname(buchname);
		this.setIsbn(isbn);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBuchname() {
		return buchname;
	}

	public void setBuchname(String buchname) {
		this.buchname = buchname;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	
	/*public boolean equals(Buch b1){
		return b1.getName().equals(name);
	}*/
	
	public int compareTo(Buch b1) {
		return this.name.compareTo(b1.getName());
	}
	
	
	
}
