package oszimt;

public class SelectSort {
	private long vertauschungen = 0;
	public static int[] intArray = { 16, 23, 14, 7, 21, 20, 6, 1, 17, 13, 12, 9, 3, 19 };

	public int[] selectsort() {
		int q, k;
		for (int i = intArray.length - 1; i >= 1; i--) {
			q = 0;
			for (int j = 1; j <= i; j++) {
				if (intArray[j] > intArray[q]) {
					q = j;
				}
			}
			k = intArray[q];
			intArray[q] = intArray[i];
			intArray[i] = k;
			inkrementVertauschungen();
		}
		return intArray;
	}

	private void inkrementVertauschungen() {
		vertauschungen++;
	}

	public long getVertauschungen() {
		return this.vertauschungen;
	}

	public static void main(String[] args) {
		SelectSort ss = new SelectSort();
		int[] array = ss.selectsort();
		for (int i = 0; i < array.length; i++) {
			System.out.println(i + 1 + ": " + array[i]);
		}
		System.out.println("Die Anzahl der Vertauschungen ist: " + ss.getVertauschungen());
	}
}
