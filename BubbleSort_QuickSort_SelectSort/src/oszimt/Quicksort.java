package oszimt;

public class Quicksort {
	private long vertauschungen = 0;
	public static int[] intArray = { 16, 23, 14, 7, 21, 20, 6, 1, 17, 13, 12, 9, 3, 19 };

	public int[] quicksort(int l, int r) {
		int q;
		if (l < r) {
			q = partition(l, r);
			quicksort(l, q);
			quicksort(q + 1, r);
			inkrementVertauschungen();
		}
		return intArray;
	}

	int partition(int l, int r) {

		int i, j, x = intArray[(l + r) / 2];
		i = l - 1;
		j = r + 1;
		while (true) {
			do {
				i++;
			} while (intArray[i] < x);

			do {
				j--;
			} while (intArray[j] > x);

			if (i < j) {
				int k = intArray[i];
				intArray[i] = intArray[j];
				intArray[j] = k;
				inkrementVertauschungen();
			} else {
				return j;
			}
			inkrementVertauschungen();
		}
	}

	private void inkrementVertauschungen() {
		vertauschungen++;
	}

	public long getVertauschungen() {
		return this.vertauschungen;
	}

	public static void main(String[] args) {
		Quicksort qs = new Quicksort();
		int[] array = qs.quicksort(0, intArray.length - 1);
		for (int i = 0; i < array.length; i++) {
			System.out.println(i + 1 + ": " + array[i]);

		}
		System.out.println("Die Anzahl der Vertauschungen ist: " + qs.getVertauschungen());
	}
}
