package de.oszimt.starsim2099;

/**
 * Write a description of class Planet here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Planet {

	// Attribute
	private double posX;
	private double posY;
	private int anzahlHafen;
	private String name;

	public Planet() {

	}

	// Methoden
	public void setPosX(double posX) {
		this.posX = posX;
	}

	public double getPosX() {
		return posX;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}

	public double getPosY() {
		return posY;
	}

	public void setanzHafen(int anzHafen) {
		this.anzahlHafen = anzHafen;
	}

	public int getanzHafen() {
		return anzahlHafen;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	// Darstellung

	public static char[][] getDarstellung() {
		char[][] planetShape = { { '\0', '/', '*', '*', '\\', '\0' }, { '|', '*', '*', '*', '*', '|' },
				{ '\0', '\\', '*', '*', '/', '\0' } };
		return planetShape;

	}
}
