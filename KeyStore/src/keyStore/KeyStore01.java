
package keyStore;

public class KeyStore01 {

	private String[] keyList;
	private int currentPos;
	public final int MAXPOSITION = 100;
	public final int NICHTGEFUNDEN = -1;

	public KeyStore01() {
		this.keyList = new String[MAXPOSITION];
		this.currentPos = 0;
	}

	public KeyStore01(int length) {
		this.keyList = new String[length];
		this.currentPos = 0;
	}

	public boolean add(String eintrag) {
		if (currentPos < MAXPOSITION) {
			this.keyList[this.currentPos] = eintrag;
			this.currentPos++;
			return true;
		} else {
			return false;
		}
	}

	public int indexOf(String eintrag) {
		// lineare Suche
		for (int i = 0; i < this.currentPos; i++) {
			if (this.keyList[i].equals(eintrag))
				return i;
		}
		return NICHTGEFUNDEN;
	}

	public void remove(int index) {
		if (index > 0 && index <= currentPos) {
			for (int i = index; i < currentPos; i++) {
				this.keyList[i] = this.keyList[i + 1];
				this.keyList[this.keyList.length - 1] = ""; //Sonderfall
			}
			currentPos--;
		}

	}

}