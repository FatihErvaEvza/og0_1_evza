package kickersTest;

import kickers.Mannschaft;
import kickers.Spieler;
import kickers.Trainer;

public class kickersTest {

	public static void main(String[] args) {

		Spieler s1 = new Spieler("Donald", "Duck", "+99456789", false, 0, 42, "Sturm");
		Spieler s2 = new Spieler("Daniel", "Düsentrieb", "+99123456", true, 0, 11010, "Tor");
		Trainer t1 = new Trainer("Mickey", "Mouse", "+99478684", true, 120, 'C', 420);
		Mannschaft m1 = new Mannschaft("Entenhausen Allstars", 'C');

		System.out.println("Spieler 1: " + s1.getVorname() + ", " +  s1.getName() + ", " + s1.getTelefon() + ", " + s1.isJahresBetragBezahlt() + ", " + s1.getJahresBetrag() + ", " + s1.getTrikotnummer() + ", " + s1.getSpielpos() );
		System.out.println("Spieler 2: " + s2.getVorname() + ", " +  s2.getName() + ", " + s2.getTelefon() + ", " + s2.isJahresBetragBezahlt() + ", " + s2.getJahresBetrag() + ", " + s2.getTrikotnummer() + ", " + s2.getSpielpos() );
		System.out.println("Manschaft : " + m1.getName() + ", " +  m1.getSpielklasse());
		System.out.println("Trainer : " + t1.getVorname() + ", " +  t1.getName() + ", " + t1.getTelefon() + ", " + t1.isJahresBetragBezahlt() + ", " + t1.getJahresBetrag() + ", " + t1.getKlasse() + ", " + t1.getJahresBetrag() );
	}

}
