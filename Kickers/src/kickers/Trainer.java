package kickers;

public class Trainer extends Person {
	private char klasse;
	private double aufwandsbetrag;
	
	public Trainer() {}
	
	public Trainer(String vorname, String name, String telefon, boolean istBezahlt, double jahresBetrag, char klasse, double betrag) {
		super(vorname, name, telefon, istBezahlt, 0);
		this.klasse = klasse;
		this.aufwandsbetrag = betrag;
	}
	
	public double getAufwandsbetrag() {
		return aufwandsbetrag;
	}
	public void setAufwandsbetrag(double aufwandsbetrag) {
		this.aufwandsbetrag = aufwandsbetrag;
	}
	public char getKlasse() {
		return klasse;
	}
	public void setKlasse(char klasse) {
		this.klasse = klasse;
	}
}
