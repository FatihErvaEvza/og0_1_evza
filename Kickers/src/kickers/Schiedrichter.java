package kickers;

public class Schiedrichter extends Person {
	private int anzahlSpiele;

	
	public Schiedrichter() {}
	
	public Schiedrichter(String vorname, String name, String telefon, boolean istBezahlt, double jahresBetrag, int anzahlSpiele) {
		super(vorname, name, telefon, istBezahlt, jahresBetrag);
		this.anzahlSpiele = anzahlSpiele;
	}
	
	public int getAnzahlSpiele() {
		return anzahlSpiele;
	}

	public void setAnzahlSpiele(int anzahlSpiele) {
		this.anzahlSpiele = anzahlSpiele;
	}
}
