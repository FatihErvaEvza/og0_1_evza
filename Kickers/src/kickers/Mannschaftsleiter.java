package kickers;

public class Mannschaftsleiter extends Spieler {
	private String mannschaft;
	private double rabatt;
	
	public Mannschaftsleiter() {}
	
	public Mannschaftsleiter(String vorname, String name, String telefon, boolean istBezahlt, double jahresBetrag, int trikotnummer, String spielpos, String mannschaft, double rabatt) {
		super(vorname,name, telefon, istBezahlt, jahresBetrag, trikotnummer, spielpos);
		this.mannschaft = mannschaft;
		this.rabatt = rabatt;
	}
	
	public String getMannschaft() {
		return mannschaft;
	}
	public void setMannschaft(String mannschaft) {
		this.mannschaft = mannschaft;
	}
	public double getRabatt() {
		return rabatt;
	}
	public void setRabatt(double rabatt) {
		this.rabatt = rabatt;
	}
}
