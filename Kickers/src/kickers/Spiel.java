package kickers;

public class Spiel {

	private String datum;
	private boolean istHeimmanschaft;
	private int toreH;
	private int toreG;
	
	public Spiel(String datum,boolean istHeimmanschaft, int toreH, int toreG) {
		this.datum = datum;
		this.istHeimmanschaft = istHeimmanschaft;
		this.toreH = toreH;
		this.toreG = toreG;
	}

	public String getDatum() {
		return datum;
	}

	public void setDatum(String datum) {
		this.datum = datum;
	}

	public boolean isIstHeimmanschaft() {
		return istHeimmanschaft;
	}

	public void setIstHeimmanschaft(boolean istHeimmanschaft) {
		this.istHeimmanschaft = istHeimmanschaft;
	}

	public int getToreH() {
		return toreH;
	}

	public void setToreH(int toreH) {
		this.toreH = toreH;
	}

	public int getToreG() {
		return toreG;
	}

	public void setToreG(int toreG) {
		this.toreG = toreG;
	}
		 

	}
