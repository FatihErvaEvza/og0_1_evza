package kickers;

public class Spieler extends Person{
	
	protected int trikotnummer;
	protected String spielpos;
	
	public Spieler() {}
	
	public Spieler(String vorname, String name, String telefon, boolean istBezahlt, double jahresBetrag, int trikotnummer, String spielpos) {
		super(vorname, name, telefon, istBezahlt, jahresBetrag);
		this.trikotnummer = trikotnummer;
		this.spielpos = spielpos;
	}
	
	public int getTrikotnummer() {
		return trikotnummer;
	}
	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}
	public String getSpielpos() {
		return spielpos;
	}
	public void setSpielpos(String spielpos) {
		this.spielpos = spielpos;
	}
}
