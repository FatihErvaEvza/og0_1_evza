package kickers;

public class Person {
	
	protected String vorname;
	protected String name;
	protected String telefon;
	protected boolean jahresBetragBezahlt;
	protected double jahresBetrag;
	
	public Person() {}
	
	public Person(String vorname, String name, String telefon, boolean istBezahlt, double jahresBetrag) {
		this.vorname = vorname;
		this.name = name;
		this.telefon = telefon;
		this.jahresBetragBezahlt = istBezahlt;
		this.jahresBetrag = jahresBetrag;
	}
	
	public String getVorname() {
		return vorname;
	}
	
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getTelefon() {
		return telefon;
	}
	
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	
	public boolean isJahresBetragBezahlt() {
		return jahresBetragBezahlt;
	}
	
	public void setJahresBetragBezahlt(boolean jahresBetragBezahlt) {
		this.jahresBetragBezahlt = jahresBetragBezahlt;
	}
	
	public double getJahresBetrag() {
		return jahresBetrag;
	}
	
	public void setJahresBetrag(double jahresBetrag) {
		this.jahresBetrag = jahresBetrag;
	}
}
