package de.futurehome.tanksimulator;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import javax.swing.*;

@SuppressWarnings("serial")
public class TankSimulator extends Frame {

	public Tank myTank;

	JFrame meinJFrame = new JFrame();

	private Label lblUeberschrift = new Label("Tank-Simulator");
	public Label lblFuellstand = new Label("     ");
	public Label lblProzente = new Label("      ");

	public Button btnBeenden = new Button("Beenden");
	public Button btnEinfuellen = new Button("Einfüllen");
	public Button btnVerbrauchen = new Button("Verbrauchen");
	public Button btnZuruecksetzen = new Button("Zurücksetzen");

	private Panel pnlNorth = new Panel();
	private Panel pnlCenter = new Panel(new FlowLayout());
	private Panel pnlSouth = new Panel(new GridLayout(1, 0));
	private Panel pnlWest = new Panel(new GridLayout(1, 0));
	

	JSlider meinSlider = new JSlider();

	private MyActionListener myActionListener = new MyActionListener(this);

	public TankSimulator() {
		super("Tank-Simulator");

		myTank = new Tank(0);
		
		meinSlider.setMinimum(0);
		meinSlider.setMaximum(4);
		meinSlider.setMajorTickSpacing(4);
		meinSlider.setMinorTickSpacing(1);
		meinSlider.createStandardLabels(1);
		meinSlider.setPaintTicks(true);
		meinSlider.setPaintLabels(true);
		meinSlider.setValue(4);
		this.lblUeberschrift.setFont(new Font("", Font.BOLD, 16));
		this.pnlNorth.add(this.lblUeberschrift);
		this.pnlCenter.add(this.lblFuellstand);
		this.pnlWest.add(this.lblProzente);
		this.pnlSouth.add(this.btnEinfuellen);
		this.pnlSouth.add(this.btnVerbrauchen);
		this.pnlSouth.add(this.btnZuruecksetzen);
		this.pnlSouth.add(this.btnBeenden);
		this.pnlCenter.add(this.meinSlider);
		this.add(this.pnlNorth, BorderLayout.NORTH);
		this.add(this.pnlCenter, BorderLayout.CENTER);
		this.add(this.pnlSouth, BorderLayout.SOUTH);
		this.add(this.pnlWest, BorderLayout.WEST);
		this.pack();
		this.setVisible(true);
		

		// Ereignissteuerung
		this.btnEinfuellen.addActionListener(myActionListener);
		this.btnVerbrauchen.addActionListener(myActionListener);
		this.btnBeenden.addActionListener(myActionListener);
		this.btnZuruecksetzen.addActionListener(myActionListener);
	}

	public static void main(String argv[]) {

		TankSimulator f = new TankSimulator();

	}
}
