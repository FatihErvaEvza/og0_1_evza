
public class Addons {

	private double preis;
	private int idnummer;
	private int anzahl;
	private int maxanzahl;
	private String name;

	public Addons() {
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public int getIdNummer() {
		return idnummer;
	}

	public void setIdNummer(int idnummer) {
		this.idnummer = idnummer;
	}

	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}

	public int getMaxAnzahl() {
		return maxanzahl;
	}

	public void setMaxAnzahl(int maxanzahl) {
		this.maxanzahl = maxanzahl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
				 this.name = name;
			 }
}